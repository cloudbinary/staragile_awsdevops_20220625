# docker-compose :
      - Run multiple Containers :

Application : School App [ Programming Language : Python & WebFramework : Django & RDBMS : Postgres ]

WebLayer:
        - WEB/APP ---> Docker image of WEB/APP  --> Run as a Container 

DB Layer:
        - DB      ---> Docker image of MYSQL    --> Run as a Container 

# -------------------------------------------- # 

STEP-1 : Create a file Dockerfile

Baked Python Image : hub.docker 

Build Tool : pip 

Create a file : requirements.txt  [ Django>=3.0,<4.0 & psycopg2>=2.8 ]

Create a file : docker-compose.yml

- Call Python Image which we have created using Dockerfile 

- Download/Pull RDBMS i.e. Postgres  from hub.docker 

- Create 
UserName : 
Password : 
DatabaseName :

- Run Python code to deploy the application in a spcific port 80:80 

- Dependency --> DB 

# -------------------------------------------- # 