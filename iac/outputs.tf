output "id" {
  value = aws_instance.web_server.id
}

output "ip" {
  value = aws_instance.web_server.public_ip 
}