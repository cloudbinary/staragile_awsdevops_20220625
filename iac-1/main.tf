# Terraform & AWS Provider Version
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  required_version = ">= 1.2.0"
}

# Authentication To AWS Provider & Region
provider "aws" {
  region  = "us-east-1"
  profile = "default"
}

# Main Code 
resource "aws_instance" "web_server" {
  ami           = var.ami
  instance_type = var.instance_type

  tags = {
    Name = "ExampleWebServerInstance"
  }
}

# Outputs
output "id" {
  value = aws_instance.web_server.id
}

output "ip" {
  value = aws_instance.web_server.public_ip
}


