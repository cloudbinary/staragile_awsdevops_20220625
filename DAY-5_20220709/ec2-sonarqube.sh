#!/bin/bash

# WebServer Of Linux - Ubuntu 22.04
aws ec2 run-instances \
--image-id "ami-068257025f72f470d" \
--instance-type "t2.xlarge" \
--count 1 \
--subnet-id "subnet-5183ac39" \
--security-group-ids "sg-0b9b6e7836e5d9765" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=SCAT-SonarQube},{Key=ProjectID,Value=20220624}]' \
--key-name "awskeys-mum" \
--user-data file://install-docker-sonarqube.txt

