#!/bin/bash

# https://www.how2shout.com/linux/install-sonarqube-on-ubuntu-20-04-18-04-server/

# Install SonarQube on Ubuntu 20.04 LTS Server

# Run Ubuntu system update
sudo apt update

# Install Java OpenJDK
sudo apt install openjdk-11-jdk

