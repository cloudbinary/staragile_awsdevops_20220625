Maven Goals :

- deploy : deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.
       
remote repository : Linux-->Jfrog-->UI--->Repositories : 

Repositories - Snapshot :
    1. http://15.206.75.148:8082/artifactory/staragile-libs-snapshot-locals/

Repositories - Release :
    1. http://15.206.75.148:8082/artifactory/staragile-libs-release-locals/

PART-1 : Jfrog Integration with SCM/VCS i.e. GitHub/GitLab/BitBucket : 

Repository : https://gitlab.com/cloudbinary/cb_java_code.git
Branch : star

Source Code : ON Jenkis --> /var/lib/jenkins/.m2/ mvn deploy 
    1. src 
    2. pom.xml 

	<distributionManagement>
		<repository>
			<id>staragile-libs-release-locals</id>
			<name>staragile-libs-release-locals</name>
			<url>http://15.206.75.148:8082/artifactory/staragile-libs-release-locals/</url>
		</repository>
		<snapshotRepository>
			<id>staragile-libs-snapshot-locals</id>
			<name>staragile-libs-snapshot-locals</name>
			<url>http://15.206.75.148:8082/artifactory/staragile-libs-snapshot-locals/</url>
		</snapshotRepository>
	</distributionManagement>


Part-2 : Jenkins Server [ https://www.jfrog.com/confluence/display/JFROG/Maven+Repository ]

STEP-1 : Do SSH To Jenkins Server [Java & Maven | environment variables ]

STEP-2 : Go to Jenkins Home path : /var/lib/jenkins/.m2

STEP-3 : Create or Find Maven home directory .m2 

    STEP-4 : Download, Install & Configure Maven on Linux Server i.e. Jenkins 

STEP-5 : Go to Maven Home path part of jenkins home : /var/lib/jenkins/.m2
    - Create 2 files :
        - settings.xml 
        - settings-security.xml 

STEP-6 : Go to Jfrog UI & Generate Repository Path 

STEP-7 : Copy the Repository Path & Credentials and update part of Jenkins Server part of Maven Home Path : /var/lib/jenkins/.m2/settings.xml 

STEP-8 : Encrypt Jfrog Password and Update unto following files settings-security.xml  settings.xml 
        - Encrypt Master Password --> settings-security.xml
        - Encrypt Password --> settings.xml

mvn --encrypt-master-password <password> --> settings-security.xml
mvn --encrypt-password <password>  ---> settings.xml



jenkins@jenkins:~/.m2$ cat settings-security.xml
<?xml version="1.0" encoding="UTF-8"?>
<settingsSecurity>
  <master></master>
</settingsSecurity>

jenkins@jenkins:~/.m2$ cat settings.xml
<?xml version="1.0" encoding="UTF-8"?>
<settings>
  <profiles>
    <profile>
      <id>default</id>
      <repositories>
        <repository>
          <id>central</id>
          <name>staragile-libs-snapshot-locals</name>
          <url>http://15.206.75.148:8082/artifactory/staragile-libs-snapshot-locals/</url>
          <releases>
            <enabled>false</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </repository>
      </repositories>
    </profile>
  </profiles>
  <activeProfiles>
    <activeProfile>default</activeProfile>
  </activeProfiles>
  <servers>
    <server>
      <id>central</id>
      <username>admin</username>
      <password></password>
    </server>
  </servers>
</settings>
