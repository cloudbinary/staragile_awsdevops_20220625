Agenda : 

Continuous Integration :
    - Jenkins   :
        - Jobs 
        - Pipeline Jobs :
            - Jenkins Scripted  : https://www.jenkins.io/doc/book/pipeline/#scripted-pipeline-fundamentals
            - Declarative Pipelines : https://www.jenkins.io/doc/book/pipeline/getting-started/
 
Maven : https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html

Binary Code Repository :
    - Jfrog 

System Requirement:
https://www.jfrog.com/confluence/display/JFROG/System+Requirements

Debian/Ubuntu:
https://www.jfrog.com/confluence/display/JFROG/Installing+Artifactory#InstallingArtifactory-DebianInstallation


Containerisation :


Static Code Analysis : SonarQube & Black Duck 

The Best Static Code Analysis Tools
SonarQube. SonarQube sample debugging error message. ...
Checkmarx SAST CxSAST. Checkmarx SAST projects scan. ...
Synopsis Coverity. Synopsis Coverity sample dashboard. ...
Micro Focus Fortify Static Code Analyzer. ...
Veracode Static Analysis. ...
Snyk Code. ...
Reshift Security.

What isthe use of static code analysis tools?

Static code analysis is a method of debugging by examining source code before a program is run. It's done by analyzing a set of code against a set (or multiple sets) of coding rules. Static code analysis and static analysis are often used interchangeably, along with source code analysis.

Example of Java :
https://www.sonarqube.org/features/multi-languages/java/#bug



Q?

Self-Hosted : Web, DB, App(Tomcat), Jenkins & Jfrog 

SaaS : Jenkins, Jfrog, Sonarqube

Pending :

SonarQube - Hands on is Pending

We are yet to start Containerisation