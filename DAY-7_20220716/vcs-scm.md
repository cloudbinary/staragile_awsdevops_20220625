# Source Control Management / Source Code Management / Version Control System : 

CLI/CUI SCM/VCS :  Git [ Linus Torvalds --> 2005 & Linux Kernel ] --> Ubuntu, Debian, RHEL, CentOS, etc...

GUI SCM/VCS : GitHub, GitLab, BitBucket, AWS CodeCommit & Azure Repos 

Software development teams to collaborate communicate in order to 
quickly solve problems and deliver new features.

What we store : Source Code / Raw Code 
- Create a secure repository to store and share your code.

What we do : Public & Private Repositories
- GitHub, GitLab, BitBucket [ Web-based Git repository hosting service ]

Private Repositories : 
- AWS CodeCommit & Azure Repos  [ Web-based Git repository hosting service ]

