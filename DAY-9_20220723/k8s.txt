K8s Cluster :
    - Deployment
    - Service Deployment
    - Replication
    - Cleanup

Kubernetes :
    - k8s-control-plane ---> Cluster 
    - node1
    - node2
    - node3 

Application : k8s Cluster ---> Nodes i.e. Node1 ---> POD ---> Container i.e. Nginx

STEP-1 : Provision 2 Linux Servers 

Pre-requisites :
1. 2 Core Processor 
2. 2 GB Ram 
3. 50 GB Hard Disk

STEP-2 : Download, Install & Configure required softwares

1. Setup Hostname                                  : k8s-controller-plane.cloudbinary.io
2. Download, Install & Configure Utility Softwares : git curl unizp tree wget
3. Install Containerization Technology             : docker.io 
4. Add Normal user(i.e. Ubuntu) unto docker group  : 
5. Change cgroup driver to systemd
6. Enable Docker Services at boot level
7. Download, & Install Kubernetes Dependencies :  apt-transport-https ca-certificates
8. Download Kubernetes GPG Signature Key: apt-key.gpg 
9. Download & Install kubelet, kubeadm and kubectl
10. Hold Auto updates on kubelet, kubeadm and kubectl
11. Restart SSM Agent on EC2 Instance
12. Attach Instance profile To EC2 Instance 



STEP-3 : Connect EC2 Instance using SSM and Validate STEP-2

STEP-4 : Create Kubernetes Cluster by going to k8s-controller-plane

Update the Host Files for 2 Servers :

vi /etc/hosts
# K8s Controller-Plane 
172.31.40.184 k8s-controller-plane.cloudbinary.io

# K8s Node-1 
172.31.34.208 k8s-node1.cloudbinary.io


## Below are the validation Commands :

kubeadm version
kubectl version
kubelet --version
docker images
docker ps
ls -lrt /etc/docker/daemon.json
cat /etc/docker/daemon.json
cat /etc/apt/sources.list.d/kubernetes.list
systemctl status docker
systemctl status kubelet




Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 172.31.40.74:6443 --token djwvgj.otn45nmm940n130f \
	--discovery-token-ca-cert-hash sha256:4f46ed31704e903a5f8ff6d43801803dd6f4f794cf74a2b500996d71e890377c 
    