# Launch EC2 Instance Of Ubuntu
aws ec2 run-instances \
    --image-id "ami-006d3995d3a6b963b" \
    --instance-type t2.medium \
    --count 1 \
    --subnet-id "subnet-5183ac39" \
    --key-name "k8skeys" \
    --security-group-ids "sg-0ae0a4da714983dd7" \
    --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=k8s-cp},{Key=Type,Value=Kubernetes Cluster - CP},{Key=ProjectName,Value=CloudBinary},{Key=Environment,Value=Dev}]' \
    --user-data file://k8s-cp.txt 