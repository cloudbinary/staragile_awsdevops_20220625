
What is continuous integration ?

- In software engineering, continuous integration is the practice of merging all developers' working copies to a shared mainline several times a day.

- Continuous integration is a DevOps software development practice where developers regularly merge their code changes into a central repository, after which automated builds and tests are run.

List of continuous integration vendors/tools ?

JENKINS
Jenkins is an open source continuous integration tool written in Java. Jenkins provides continuous integration services for software development. It is a server-based system running in a servlet container such as Apache Tomcat. It supports SCM tools including AccuRev CVS Subversion Git Mercurial Perforce Clearcase and RTC and can execute Apache Ant and Apache Maven based projects as well as arbitrary shell scripts and Windows batch commands.

TRAVIS CI
Travis CI is an open-source hosted distributed continuous integration service used to build and test projects hosted at GitHub. Travis CI is configured by adding a file named .travis.yml which is a YAML format text file to the root directory of the GitHub repository.

CIRCLECI
CircleCI is the world\'92s largest shared continuous integration and continuous delivery (CI/CD) platform and the central hub where code moves from idea to delivery. As one of the most-used DevOps tools CircleCI has unique access to data on how engineering teams work and how their code runs.

AZURE DEVOPS CODE
Azure DevOps provides developer services to support teams to plan work, collaborate on code development, and build and deploy applications. Developers can work in the cloud using Azure DevOps Services or on-premises using Azure DevOps Server. Azure DevOps Server was formerly named Visual Studio Team Foundation Server (TFS).

AWS CODEBUILD
AWS CodeBuild is a fully managed build service that compiles source code runs tests and produces software packages that are ready to deploy. With CodeBuild you don\'92t need to provision manage and scale your own build servers. CodeBuild scales continuously and processes multiple builds concurrently so your builds are not left waiting in a queue. You can get started quickly by using prepackaged build environments or you can create custom build environments that use your own build tools. With CodeBuild you are charged by the minute for the compute resources you use.

GITLAB CI
GitLab CI is a part of GitLab a web application with an API that stores its state in a database. It manages projects/builds and provides a nice user interface besides all the features of GitLab. GitLab Runner is an application which processes builds. It can be deployed separately and works with GitLab CI through an API. In order to run tests you need at least one GitLab instance and one GitLab Runner.

GitHub Actions

ATLASSIAN BAMBOO
Bamboo is a continuous integration server from Atlassian the makers of JIRA Confluence and Crowd. Bamboo supports builds in any programming language using any build tool including Ant Maven make and any command line tools. Build notifications can be customized based on the type of event and received via email instant message RSS or pop-up windows in Eclipse-based IDEs and IntelliJ IDEA.


Explore unto Jenkins

https://www.jenkins.io/blog/2017/02/07/declarative-maven-project/

